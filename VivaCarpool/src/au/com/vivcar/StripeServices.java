package au.com.vivcar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.ChargeCollection;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCardCollection;
import com.stripe.model.ExternalAccountCollection;
import com.stripe.model.Token;

public class StripeServices {

	public static String stripeApiKey = "sk_test_66sQPRegWObX2TKAyR17clqK";
	
	public static String createStripeCustomer(String email){
			
			Stripe.apiKey = stripeApiKey;
			
			Map<String, Object> customerParams = new HashMap<String, Object>();
			customerParams.put("email", email);
			customerParams.put("account_balance","0");
			
			Customer customer;
			String stripeCustomerId ="";
			
			try {
				customer = Customer.create(customerParams);
				stripeCustomerId = customer.getId();
				
				
			} catch (AuthenticationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidRequestException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (APIConnectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CardException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (APIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return stripeCustomerId;
		}
	
	public static void createCardForCustomer(String custid, String number, String month, String year, String cvc){
		
		Stripe.apiKey = stripeApiKey;
		
		Map<String, Object> tokenParams = new HashMap<String, Object>();
		Map<String, Object> cardParams = new HashMap<String, Object>();
		cardParams.put("number", number);
		cardParams.put("exp_month", month);
		cardParams.put("exp_year", year);
		cardParams.put("cvc", cvc);
		tokenParams.put("card", cardParams);

	
		try {
			Token token = Token.create(tokenParams);
			System.out.println(token.getId());
			Customer cust = Customer.retrieve(custid);
			cust.createCard(token.getId());
		
			
			
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
public static String createCardForCustomer(String token, String custId){
		
		Stripe.apiKey = stripeApiKey;
		String cardId = "";
		
		try {
			
			System.out.println(token);
			Customer cust = Customer.retrieve(custId);
			Card card = cust.createCard(token);
			
			cardId = card.getId();
		
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cardId;
	}

	public static String seatBookingCharge(String custId, String rideId){
		
		Stripe.apiKey = "sk_test_66sQPRegWObX2TKAyR17clqK";
		
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", 600);
		chargeParams.put("currency", "aud");
		chargeParams.put("description", "Charge for booking seats for rideid : " + rideId);
		
		try {
			//Customer user = Customer.retrieve(custId);
			
			chargeParams.put("customer", custId);
			
			Charge bookingcharge = Charge.create(chargeParams);
			
			System.out.println("Payment Id " + bookingcharge.getId());
			return bookingcharge.getId();
			
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
public static List<Charge> getPaymentHistory(String custId){
		
		Stripe.apiKey = "sk_test_66sQPRegWObX2TKAyR17clqK";
		
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("customer", custId);

		try {
			ChargeCollection collectionOfCharges = Charge.list(chargeParams);
			List<Charge> listOfCharges = collectionOfCharges.getData();
			
			return listOfCharges;
			
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

public static Card getCardDetails(String custId){
	
	Stripe.apiKey = "sk_test_66sQPRegWObX2TKAyR17clqK";
	
	Map<String, Object> cardParams = new HashMap<String, Object>();
	cardParams.put("limit", 3);
	cardParams.put("object", "card");
	
	
	try {
		ExternalAccountCollection eac = Customer.retrieve(custId).getSources().all(cardParams);
		
		//System.out.println(eac.getCount());
		System.out.println(eac);
		Card c = (Card)eac.getData().get(0);
		System.out.println(c);
		return c;
		
		
	} catch (AuthenticationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidRequestException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (APIConnectionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CardException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (APIException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
	
}

public static String makePayment(String custId, String rideId){
	
	Stripe.apiKey = "sk_test_66sQPRegWObX2TKAyR17clqK";
	
	Map<String, Object> chargeParams = new HashMap<String, Object>();
	chargeParams.put("amount", 600);
	chargeParams.put("currency", "aud");
	chargeParams.put("description", "Charge for booking seats for rideid : " + rideId);
	
	try {
		//Customer user = Customer.retrieve(custId);
		
		chargeParams.put("customer", custId);
		
		Charge bookingcharge = Charge.create(chargeParams);
		
		System.out.println("Payment Id " + bookingcharge.getId());
		return bookingcharge.getId();
		
	} catch (AuthenticationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidRequestException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (APIConnectionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (CardException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (APIException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
	
}

}
