package au.com.vivcar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.ArangoDatabase;
import com.arangodb.entity.BaseDocument;
import com.arangodb.entity.CollectionEntity;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.model.DocumentReadOptions;
import com.arangodb.util.MapBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.StripeException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.net.RequestOptions;
import com.stripe.net.RequestOptions.RequestOptionsBuilder;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import jdk.nashorn.internal.runtime.options.Options;

@Path("/")
public class JSONService {

	//local
	ArangoDB arangoDB = new ArangoDB.Builder().user("root").password("Hsrada2789").build();
	//server
	//ArangoDB arangoDB = new ArangoDB.Builder().user("root").password("hsrada27").build();
	String dbName = "vivacarpool";

	String firebaseServerKey = "AAAAr9qGESk:APA91bEvBIqpv7VbHJD-p1w022fhn5GEt6E3TvNBgSESoJVfUtmmMvMdALzOhR9gq6L0b7iqNM_SeoNtgfQ3o19TpX0ToMZRkoucFfeiSlRlxwIAj7EAQIncIXV6DbNZL2l1CfBGkr5X";
	
	String profilePicPath = "http://128.199.126.197:8080/vivacarprofile/";
	String dlImagePath = "http://128.199.126.197:8080/vivacardl/";
	
	String profilePicFilePath ="/home/techneeds/vivacar/profileimages";
	String dlImagesFilePath ="/home/techneeds/vivacar/dlimages";
	
	@GET
	@Path("test")
	@Produces(MediaType.APPLICATION_JSON)
	public Track getTrackInJSON() {

		Track track = new Track();
		track.setTitle("Enter Sandman");
		track.setSinger("Metallica");

		return track;

	}
	
	@GET
	@Path("testdb")
	@Produces(MediaType.APPLICATION_JSON)
	public Track getTrack() {
		try {
		  
		  CollectionEntity myArangoCollection = arangoDB.db(dbName).createCollection(Constants.TABLE_RIDES);
		  System.out.println("Collection created: " + myArangoCollection.getName());
		  
		} catch (ArangoDBException e) {
		  System.err.println("Failed to create database: " + dbName + "; " + e.getMessage());
		}
		
		
		return null;

	}
	
	@POST
	@Path("/signin")
	@Produces(MediaType.APPLICATION_JSON)
	public Response signin(
		@FormParam("email") String email,
		@FormParam("password") String password) {

		JSONObject response =  new JSONObject();
		
		try {
			
			  String query = "FOR u IN users FILTER u.email == @email AND u.password == @password RETURN u";
			  Map<String, Object> bindVars = new MapBuilder().put("email", email).put("password", password).get();
			  ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			  if(cursor==null){
				  System.out.println("Empty cursor");
			  }
			  else{
				  if(cursor.hasNext()){
					  BaseDocument document = cursor.next();
						try {
							response.put("success", true);
							response.put("message","Login Successful");
							response.put("name",document.getAttribute("name"));
							response.put("profilepic",document.getAttribute("profile_pic"));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				  }
				  else{
					  
						try {
							response.put("success", false);
							response.put("message","Email id / Password incorrect");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				  }
				  for (; cursor.hasNext();) {
							System.out.println("Key: " + cursor.next().getKey());
				  }
				  
			  }
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			}
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/signup")
	@Produces(MediaType.APPLICATION_JSON)
	public Response signup(
		@FormParam("email") String email,
		@FormParam("name") String name,
		@FormParam("mobile") String mobile,
		@FormParam("registered_on") String registeredOn,
		@FormParam("activated_on") String activatedOn,
		@FormParam("last_login") String lastLogin,
		@FormParam("status") String status,
		@FormParam("is_email_verified") String emailVerified,
		@FormParam("dl_pic") String dlImage,
		@FormParam("profile_pic") String profileImage,
		@FormParam("password") String password,
		@FormParam("firebaseid") String firebaseId) {

		BaseDocument user = new BaseDocument();
		user.addAttribute("name", name);
		user.addAttribute("email", email);
		user.addAttribute("mobile", mobile);
		user.addAttribute("password", password);
		user.addAttribute("profile_pic", profilePicPath+"default.jpg");
		user.addAttribute("dl_image", dlImage);
		user.addAttribute("registered_on", registeredOn);
		user.addAttribute("activated_on", activatedOn);
		user.addAttribute("last_login", lastLogin);
		user.addAttribute("email_verified", emailVerified);
		user.addAttribute("status", status);
		user.addAttribute("firebaseid", firebaseId);
		user.addAttribute("stripeid", "");
		user.addAttribute("cardid", "");
		
		
		
		JSONObject response =  new JSONObject();
		
		try {
			  DocumentCreateEntity<BaseDocument> doc = arangoDB.db(dbName).collection(Constants.TABLE_USERS).insertDocument(user);
			  
			  String userKey = doc.getKey();
			  
			  String stripeCustId = StripeServices.createStripeCustomer(email);
			  
			  user = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(userKey,BaseDocument.class);
			  
			  System.out.println(user.getKey());
			  
			  user.updateAttribute("stripeid", stripeCustId);
				
			  arangoDB.db(dbName).collection(Constants.TABLE_USERS).updateDocument(userKey, user);
					
				
				try {
					response.put("success", true);
					response.put("message","Registered Successfully");
					response.put("name",user.getAttribute("name"));
					response.put("profilepic",user.getAttribute("profile_pic"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  System.out.println("Document created");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error during registration. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/updatetoken")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateToken(
		@FormParam("email") String email,
		@FormParam("token") String token) {
		
		JSONObject response =  new JSONObject();
		System.out.println(token);
		try {
			
			String userKey = getUserKey(email);
			
			BaseDocument user = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(userKey,BaseDocument.class);
			
			
			String custid = user.getAttribute("stripeid").toString();
			
			System.out.println(custid);
			
			String cardId = StripeServices.createCardForCustomer(token,custid);
			
			user.updateAttribute("cardid", cardId);
			
			arangoDB.db(dbName).collection(Constants.TABLE_USERS).updateDocument(userKey, user);
			 
			response.put("success", true);
			response.put("message","Registered Successfully");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			try {
				response.put("success", false);
				response.put("message","Error during registration. Please try again");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		}
		
		return Response.status(200)
				.entity(response)
				.build();
	}
	
	@POST
	@Path("/updatecard")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCardDetails(
		@FormParam("email") String email,
		@FormParam("number") String number,
		@FormParam("month") String month,
		@FormParam("year") String year,
		@FormParam("cvc") String cvc) {

		
		JSONObject response =  new JSONObject();
		
		try {
			 
			String userKey = getUserKey(email);
			
			BaseDocument user = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(userKey,BaseDocument.class);
			
			String custid = user.getAttribute("stripeid").toString();
			
			System.out.println(custid);
			
			StripeServices.createCardForCustomer(custid, number, month, year, cvc);
					
				
				try {
					response.put("success", true);
					response.put("message","Registered Successfully");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  System.out.println("Document created");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error during registration. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
		@FormDataParam("profilepic") InputStream uploadedInputStream,
		@FormDataParam("profilepic") FormDataContentDisposition fileDetail) {

		System.out.println("Upload method");
		String uploadedFileLocation = "/home/techneeds/airmeals/dishimages/" + "abc.jpg";

		writeToFile(uploadedInputStream, uploadedFileLocation);

		String output = "File uploaded to : " + uploadedFileLocation;

		System.out.println(output);

		return Response.status(200).entity(output).build();

	}


	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {

		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	
	@POST
	@Path("/searchrides")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchRides(
		@FormParam("from") String from,
		@FormParam("to") String to,
		@FormParam("date") String date,
		@FormParam("time") String time) throws JSONException {

		JSONObject response =  new JSONObject();
		
		String status = "Upcoming";
		
		System.out.println("From - " + from +"; To - " + to + " date = "+date);
		try {
			String query ="";
			Map<String, Object> bindVars, userVars;
			if(date ==null || date.equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	            Date today = new Date();

	            String formattedDate = sdf.format(today);
	            
				System.out.println("In here = " + formattedDate);
				query = "FOR r IN rides FILTER (r.from == @from OR r.stop_over like @stopover) AND r.to == @to  AND r.time >= @time AND r.date >= @date AND r.seats_available > @seats AND r.status == @status RETURN r";
				
				bindVars = new MapBuilder().put("from", from).put("to",to).put("time",time).put("seats","0").put("date",formattedDate).put("stopover","%"+from+"%").put("status",status).get();
				
			}
			else{
				System.out.println("In else");
				query = "FOR r IN rides FILTER (r.from == @from OR r.stop_over like @stopover) AND r.to == @to AND r.time >= @time AND r.date == @date AND r.seats_available > @seats AND r.status == @status RETURN r";
				bindVars = new MapBuilder().put("from", from).put("to", to).put("time", "00:00").put("date",date).put("seats","0").put("stopover","%"+from+"%").put("status",status).get();
				
			}
			
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray ridesArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
				JSONObject ride = new JSONObject();
				BaseDocument document = cursor.next();
				System.out.println(document.getAttribute("date") +" " + document.getAttribute("time"));
				ride.put("rideid", document.getKey());
				ride.put("from", document.getAttribute("from"));
				ride.put("to", document.getAttribute("to"));
				//ride.put("stopover", document.getAttribute("stop_over"));
				ride.put("onward_date", document.getAttribute("date") +" " + document.getAttribute("time"));
				ride.put("ac", document.getAttribute("ac"));
				ride.put("pets", document.getAttribute("pets"));
				ride.put("music", document.getAttribute("music"));
				ride.put("posted_on", document.getAttribute("posted_on"));
				ride.put("posted_by_email", document.getAttribute("posted_by"));
				ride.put("seats", document.getAttribute("seats_available"));
				ride.put("price", document.getAttribute("price"));
				
				String userQuery = "FOR u IN users FILTER u.email == @email RETURN u";
				userVars = new MapBuilder().put("email", document.getAttribute("posted_by")).get();
				ArangoCursor<BaseDocument> userCursor = arangoDB.db(dbName).query(userQuery, userVars, null,
					      BaseDocument.class);
				
				if(userCursor.hasNext()){
					while(userCursor.hasNext()){
						ride.put("posted_by", userCursor.next().getAttribute("name"));
					}
				}
				
				ridesArray.put(ride);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("rides", ridesArray);
			
			}
			else{
				response.put("success", false);
				response.put("message","Ride Not present");
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/addride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRide(
		@FormParam("from") String from,
		@FormParam("to") String to,
		@FormParam("date") String date,
		@FormParam("time") String time,
		@FormParam("seats") String seats,
		@FormParam("price") String price,
		@FormParam("pay_by") String paymentMethod,
		@FormParam("stop_over") String stopOver,
		@FormParam("owner_comments") String comments,
		@FormParam("car_number") String carNumber,
		@FormParam("car_model") String carModel,
		@FormParam("car_color") String carColor,
		@FormParam("ac") String ac,
		@FormParam("music") String music,
		@FormParam("pets") String pets,
		@FormParam("luggage") String luggage,
		@FormParam("posted_by") String postedBy,
		@FormParam("posted_on") String postedOn,
		@FormParam("status") String status) {

		BaseDocument ride = new BaseDocument();
		ride.addAttribute("posted_by", postedBy);
		ride.addAttribute("posted_on", postedOn);
		ride.addAttribute("from", from);
		ride.addAttribute("to", to);
		ride.addAttribute("date", date);
		ride.addAttribute("time", time);
		ride.addAttribute("seats", seats);
		ride.addAttribute("seats_available", seats);
		ride.addAttribute("price", price);
		ride.addAttribute("pay_by", paymentMethod);
		ride.addAttribute("stop_over", stopOver);
		if(comments =="" || comments == null){
			comments = "No comments from owner";
		}
		ride.addAttribute("owner_comments", comments);
		ride.addAttribute("car_number", carNumber);
		ride.addAttribute("car_model", carModel);
		ride.addAttribute("car_color", carColor);
		ride.addAttribute("ac", ac);
		ride.addAttribute("pets", pets);
		ride.addAttribute("luggage", luggage);
		ride.addAttribute("music", music);
		ride.addAttribute("status", status);
		
		
		JSONObject response =  new JSONObject();
		
		try {
			  arangoDB.db(dbName).collection("rides").insertDocument(ride);
			  
				try {
					response.put("success", true);
					response.put("message","Ride Added Successfully");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  System.out.println("Document created");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error during adding ride. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		
		
		return Response.status(200)
			.entity(response)
			.build();


	}
	
	@POST
	@Path("/updateride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRide(
		@FormParam("rideid") String rideId,
		@FormParam("from") String from,
		@FormParam("to") String to,
		@FormParam("date") String date,
		@FormParam("time") String time,
		@FormParam("seats") String seats,
		@FormParam("price") String price,
		@FormParam("pay_by") String paymentMethod,
		@FormParam("stop_over") String stopOver,
		@FormParam("owner_comments") String comments,
		@FormParam("car_number") String carNumber,
		@FormParam("car_model") String carModel,
		@FormParam("car_color") String carColor,
		@FormParam("ac") String ac,
		@FormParam("music") String music,
		@FormParam("pets") String pets,
		@FormParam("luggage") String luggage,
		@FormParam("posted_by") String postedBy,
		@FormParam("posted_on") String postedOn,
		@FormParam("status") String status) {

		BaseDocument ride = arangoDB.db(dbName).collection("rides").getDocument(rideId,BaseDocument.class);
		ride.updateAttribute("posted_by", postedBy);
		ride.updateAttribute("posted_on", postedOn);
		ride.updateAttribute("from", from);
		ride.updateAttribute("to", to);
		ride.updateAttribute("date", date);
		ride.updateAttribute("time", time);
		ride.updateAttribute("seats", seats);
		ride.updateAttribute("seats_available", seats);
		ride.updateAttribute("price", price);
		ride.updateAttribute("pay_by", paymentMethod);
		ride.updateAttribute("stop_over", stopOver);
		if(comments =="" || comments == null){
			comments = "No comments from owner";
		}
		ride.updateAttribute("owner_comments", comments);
		ride.updateAttribute("car_number", carNumber);
		ride.updateAttribute("car_model", carModel);
		ride.updateAttribute("car_color", carColor);
		ride.updateAttribute("ac", ac);
		ride.updateAttribute("pets", pets);
		ride.updateAttribute("luggage", luggage);
		ride.updateAttribute("music", music);
		ride.updateAttribute("status", status);
		
		
		JSONObject response =  new JSONObject();
		
		try {
			  
			  arangoDB.db(dbName).collection("rides").updateDocument(ride.getKey(), ride);
				try {
					response.put("success", true);
					response.put("message","Ride Updated Successfully");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  System.out.println("Document created");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error during adding ride. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		
		
		return Response.status(200)
			.entity(response)
			.build();


	}
	
	@POST
	@Path("/ridedetail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRideDetail(
		@FormParam("rideid") String rideId) throws JSONException {

		System.out.println("Ride id = "+rideId);
		JSONObject response =  new JSONObject();
		
		try {
			
			String query ="";
			Map<String, Object> bindVars, userVars;
			
				query = "FOR r IN rides FILTER r._key == @rideid RETURN r";
				
				bindVars = new MapBuilder().put("rideid", rideId).get();
				
			
			
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray ridesArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
			
				JSONObject ride = new JSONObject();
				BaseDocument document = cursor.next();
				
				ride.put("rideid", document.getKey());
				ride.put("from", document.getAttribute("from"));
				ride.put("to", document.getAttribute("to"));
				ride.put("stop_over", document.getAttribute("stop_over"));
				ride.put("onward_date", document.getAttribute("date") +" " + document.getAttribute("time"));
				ride.put("owner_comments", document.getAttribute("owner_comments"));
				ride.put("car_model", document.getAttribute("ac"));
				ride.put("car_number", document.getAttribute("ac"));
				ride.put("car_color", document.getAttribute("ac"));
				ride.put("luggage", document.getAttribute("luggage"));
				ride.put("pay_by", document.getAttribute("pay_by"));
				ride.put("ac", document.getAttribute("ac"));
				ride.put("pets", document.getAttribute("pets"));
				ride.put("music", document.getAttribute("music"));
				ride.put("posted_on", document.getAttribute("posted_on"));
				ride.put("posted_by_email", document.getAttribute("posted_by"));
				ride.put("seats", document.getAttribute("seats_available"));
				ride.put("price", document.getAttribute("price"));
				ride.put("status", document.getAttribute("status"));
				
				String userQuery = "FOR u IN users FILTER u.email == @email RETURN u";
				userVars = new MapBuilder().put("email", document.getAttribute("posted_by")).get();
				ArangoCursor<BaseDocument> userCursor = arangoDB.db(dbName).query(userQuery, userVars, null,
					      BaseDocument.class);
				
				if(userCursor.hasNext()){
					while(userCursor.hasNext()){
						ride.put("posted_by", userCursor.next().getAttribute("name"));
					}
				}
				
				ridesArray.put(ride);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("rides", ridesArray);
			
			}
			else{
				response.put("success", false);
				response.put("message","Ride Not present");
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/bookseats")
	@Produces(MediaType.APPLICATION_JSON)
	public Response bookRide(
		@FormParam("rideid") String rideId,
		@FormParam("bookedby") String bookedBy,
		@FormParam("bookedon") String bookedOn,
		@FormParam("seats") String seats) {

		BaseDocument rideBooking = new BaseDocument();
		rideBooking.addAttribute("rideid", rideId);
		rideBooking.addAttribute("booked_on", bookedOn);
		rideBooking.addAttribute("booked_by", bookedBy);
		rideBooking.addAttribute("seats", seats);
		rideBooking.addAttribute("status", "Waiting for Confirmation");
		rideBooking.addAttribute("paymentstatus", "Pending");
		rideBooking.addAttribute("review", "Pending");
		
		
		JSONObject response =  new JSONObject();
		
		try {
			  arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).insertDocument(rideBooking);
			  
			  BaseDocument userDocument = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(getUserKey(bookedBy),BaseDocument.class);
			  
			  String paymentId = StripeServices.seatBookingCharge(userDocument.getAttribute("stripeid").toString(), rideId);
			  
			  try {
					response.put("success", true);
					response.put("message","Ride Booked Successfully");
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  System.out.println("Document created");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error while booking ride. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		BaseDocument rideDocument = arangoDB.db(dbName).collection("rides").getDocument(rideId,BaseDocument.class);
		Integer seatsCount = Integer.valueOf(rideDocument.getAttribute("seats_available").toString());
		Integer bookedSeats = Integer.valueOf(seats);
		Integer updatedSeats = seatsCount - bookedSeats;
		
		rideDocument.updateAttribute("seats_available", updatedSeats.toString());
		
		try {
			arangoDB.db(dbName).collection("rides").updateDocument(rideId, rideDocument);
			
			String postedBy = rideDocument.getAttribute("posted_by").toString();
			String firebaseId = getFirebaseId(postedBy);
			
			sendNotification(postedBy, seats+" seat(s) have been reserved for ride id - " + rideId, "Seat Booking", firebaseId);
			addNotificationToDB(postedBy, "Seat Booking", seats+" seat(s) have been reserved for ride id - " + rideId, rideId, firebaseId);
			
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/getridesastraveller")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRideHistoryAsTraveller(
		@FormParam("useremail") String email) throws JSONException {
System.out.println("Req reeceived");
		JSONObject response =  new JSONObject();
		
		try {
			
			String query ="";
			Map<String, Object> bindVars, userVars;
			
				query = "FOR r IN ridebookings FILTER r.booked_by == @email RETURN r";
				
				bindVars = new MapBuilder().put("email", email).get();
				
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray rideBookingsArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
			
				JSONObject rideBooking = new JSONObject();
				BaseDocument document = cursor.next();
				
				rideBooking.put("bookingid", document.getKey());
				rideBooking.put("rideid", document.getAttribute("rideid"));
				rideBooking.put("seats", document.getAttribute("seats"));
				rideBooking.put("booked_on", document.getAttribute("booked_on"));
				rideBooking.put("status", document.getAttribute("status"));
				rideBooking.put("paymentstatus", document.getAttribute("status"));
				rideBooking.put("review", document.getAttribute("review"));
				
				
				String rideQuery = "FOR s IN rides FILTER s._key == @rideid RETURN s";
				userVars = new MapBuilder().put("rideid", document.getAttribute("rideid")).get();
				
				ArangoCursor<BaseDocument> userCursor = arangoDB.db(dbName).query(rideQuery, userVars, null,
					      BaseDocument.class);
				
				if(userCursor.hasNext()){
					while(userCursor.hasNext()){
						BaseDocument rideDocument = userCursor.next();
						rideBooking.put("from", rideDocument.getAttribute("from"));
						rideBooking.put("to", rideDocument.getAttribute("to"));
						rideBooking.put("payby", rideDocument.getAttribute("pay_by"));
						rideBooking.put("price", rideDocument.getAttribute("price"));
						rideBooking.put("date", rideDocument.getAttribute("date")+" "+rideDocument.getAttribute("time"));
					}
				}
				
				rideBookingsArray.put(rideBooking);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("rides", rideBookingsArray);
			
			}
			else{
				response.put("success", false);
				response.put("rides", rideBookingsArray);
				response.put("message","Ride Not present");
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/getridesasowner")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRiedHistoryAsOwner(
		@FormParam("useremail") String email) throws JSONException {

		JSONObject response =  new JSONObject();
		
		try {
			
			String query ="";
			Map<String, Object> bindVars, userVars;
			
				query = "FOR r IN rides FILTER r.posted_by == @email RETURN r";
				
				bindVars = new MapBuilder().put("email", email).get();
				
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray rideBookingsArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
			
				JSONObject rideBooking = new JSONObject();
				BaseDocument document = cursor.next();
				
				rideBooking.put("rideid", document.getKey());
				rideBooking.put("from", document.getAttribute("from"));
				rideBooking.put("to", document.getAttribute("to"));
				rideBooking.put("date", document.getAttribute("date")+" "+document.getAttribute("time"));
				rideBooking.put("seats", document.getAttribute("seats"));
				rideBooking.put("seats_available", document.getAttribute("seats_available"));
				rideBooking.put("status", document.getAttribute("status"));
				
				
				rideBookingsArray.put(rideBooking);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("rides", rideBookingsArray);
			
			}
			else{
				response.put("success", false);
				response.put("message","Ride Not present");
				response.put("rides", rideBookingsArray);
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/bookingsforride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBookingsForRide(
		@FormParam("rideid") String rideId) throws JSONException {

		JSONObject response =  new JSONObject();
		
		System.out.println("Reservations = " + rideId);
		try {
			
			String query ="";
			Map<String, Object> bindVars, userVars;
			
				query = "FOR r IN ridebookings FILTER r.rideid == @rideid RETURN r";
				
				bindVars = new MapBuilder().put("rideid", rideId).get();
				
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray rideBookingsArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
			
				JSONObject rideBooking = new JSONObject();
				BaseDocument document = cursor.next();
				
				rideBooking.put("rideid", rideId);
				rideBooking.put("booked_by_email", document.getAttribute("booked_by"));
				rideBooking.put("booked_on", document.getAttribute("booked_on"));
				rideBooking.put("seats", document.getAttribute("seats"));
				rideBooking.put("status", document.getAttribute("seats"));
				
				
				String userQuery = "FOR u IN users FILTER u.email == @email RETURN u";
				userVars = new MapBuilder().put("email", document.getAttribute("booked_by")).get();
				ArangoCursor<BaseDocument> userCursor = arangoDB.db(dbName).query(userQuery, userVars, null,
					      BaseDocument.class);
				
				if(userCursor.hasNext()){
					while(userCursor.hasNext()){
						BaseDocument doc2 = userCursor.next();
						rideBooking.put("posted_by", doc2.getAttribute("name"));
						rideBooking.put("booked_user_pic", doc2.getAttribute("profile_pic"));
					}
				}
				
				rideBookingsArray.put(rideBooking);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("rides", rideBookingsArray);
			
			}
			else{
				response.put("success", false);
				response.put("message","Ride Not present");
				response.put("rides", rideBookingsArray);
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/cancelride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelRide(
		@FormParam("rideid") String rideId) throws JSONException {

		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection("rides").getDocument(rideId,BaseDocument.class);
		Map<String, Object> bookingVars;
		
		rideDocument.updateAttribute("status", "Cancelled");
		
		try {
			arangoDB.db(dbName).collection("rides").updateDocument(rideId, rideDocument);
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  response.put("success", false);
				response.put("message","Error while cancelling ride. Please try again");	
			}
		
		String rideBookingQuery = "FOR r IN ridebookings FILTER r.rideid == @rideid RETURN r";
		bookingVars = new MapBuilder().put("rideid", rideId).get();
		ArangoCursor<BaseDocument> rideBookingCusor = arangoDB.db(dbName).query(rideBookingQuery, bookingVars, null,
			      BaseDocument.class);
		
		if(rideBookingCusor.hasNext()){
			while(rideBookingCusor.hasNext()){
				BaseDocument rideBookingDocument = rideBookingCusor.next();
				rideBookingDocument.updateAttribute("status", "Cancelled by Owner");
				try {
					arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideBookingDocument.getKey(), rideBookingDocument);
					response.put("success", true);
					response.put("message","Ride Cancelled Successfully");
					
					String bookedBy = rideBookingDocument.getAttribute("booked_by").toString();
					String firebaseId = getFirebaseId(bookedBy);
					
					sendNotification(bookedBy, "Ride - "+rideId+ " has been cancelled by the owner", "Ride Cancelled", firebaseId);
					addNotificationToDB(bookedBy, "Ride Cancelled", "Ride - "+rideId+ " has been cancelled by the owner", rideId, firebaseId);
					
					} catch (ArangoDBException e) {
					  System.err.println("Failed to update document. " + e.getMessage());
					  	response.put("success", false);
						response.put("message","Error while cancelling ride. Please try again");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		}
		else{
			response.put("success", true);
			response.put("message","Ride Cancelled Successfully");
		}
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	@POST
	@Path("/cancelbooking")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelBooking(
		@FormParam("bookingid") String bookingId) throws JSONException {

		System.out.println("booking"+bookingId);
		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).getDocument(bookingId,BaseDocument.class);
		System.out.println(rideDocument.getKey());
		Map<String, Object> bookingVars;
		String seatsBooked = rideDocument.getAttribute("seats").toString();
		String rideId = rideDocument.getAttribute("rideid").toString();
		rideDocument.updateAttribute("status", "Cancelled");
		
		try {
			arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideDocument.getKey(), rideDocument);
			response.put("success", true);
			response.put("message","Booking Cancelled Successfully");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  	response.put("success", false);
				response.put("message","Error while cancelling ride. Please try again");
			}
		
		String rideBookingQuery = "FOR r IN rides FILTER r._key == @rideid RETURN r";
		bookingVars = new MapBuilder().put("rideid", rideId).get();
		ArangoCursor<BaseDocument> rideBookingCusor = arangoDB.db(dbName).query(rideBookingQuery, bookingVars, null,
			      BaseDocument.class);
		
		if(rideBookingCusor.hasNext()){
			while(rideBookingCusor.hasNext()){
				BaseDocument rideBookingDocument = rideBookingCusor.next();
				
				Integer newSeatCount = Integer.parseInt(rideBookingDocument.getAttribute("seats").toString()) + Integer.parseInt(seatsBooked);
				
				rideBookingDocument.updateAttribute("seats", newSeatCount.toString());
				try {
					arangoDB.db(dbName).collection("rides").updateDocument(rideBookingDocument.getKey(), rideBookingDocument);
					response.put("success", true);
					response.put("message","Booking Cancelled Successfully");
					
					String postedBy = rideBookingDocument.getAttribute("posted_by").toString();
					String firebaseId = getFirebaseId(postedBy);
					
					sendNotification(postedBy, "User has cancelled the booking for Ride - "+rideId, "Booking Cancellation", firebaseId);
					addNotificationToDB(postedBy, "Booking Cancellation","User has cancelled the booking for Ride - "+rideId, rideId, firebaseId);
					
					
					} catch (ArangoDBException e) {
					  System.err.println("Failed to update document. " + e.getMessage());
					  	response.put("success", false);
						response.put("message","Error while cancelling ride. Please try again");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		}
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	@POST
	@Path("/rejectreservation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectReservation(
		@FormParam("bookingid") String bookingId) throws JSONException {

		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).getDocument(bookingId,BaseDocument.class);
		
		Map<String, Object> bookingVars;
		String seatsBooked = rideDocument.getAttribute("seats").toString();
		String rideId = rideDocument.getAttribute("rideid").toString();
		rideDocument.updateAttribute("status", "Reservation rejected by Ride poster");
		
		try {
			arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideDocument.getKey(), rideDocument);
			response.put("success", true);
			response.put("message","Booking Cancelled Successfully");
			
			String bookedBy = rideDocument.getAttribute("booked_by").toString();
			String firebaseId = getFirebaseId(bookedBy);
			
			sendNotification(bookedBy, "The owner has rejected your reservstion for ride - "+rideId, "Booking Cancellation", firebaseId);
			addNotificationToDB(bookedBy, "Booking Cancellation","The owner has rejected your reservstion for ride - "+rideId, rideId, firebaseId);
			
			
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  	response.put("success", false);
				response.put("message","Error while cancelling ride. Please try again");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		String rideBookingQuery = "FOR r IN rides FILTER r._key == @rideid RETURN r";
		bookingVars = new MapBuilder().put("rideid", rideId).get();
		ArangoCursor<BaseDocument> rideBookingCusor = arangoDB.db(dbName).query(rideBookingQuery, bookingVars, null,
			      BaseDocument.class);
		
		if(rideBookingCusor.hasNext()){
			while(rideBookingCusor.hasNext()){
				BaseDocument rideBookingDocument = rideBookingCusor.next();
				
				Integer newSeatCount = Integer.parseInt(rideBookingDocument.getAttribute("seats").toString()) + Integer.parseInt(seatsBooked);
				
				rideBookingDocument.updateAttribute("seats", newSeatCount.toString());
				try {
					arangoDB.db(dbName).collection("rides").updateDocument(rideBookingDocument.getKey(), rideBookingDocument);
					response.put("success", true);
					response.put("message","Booking Cancelled Successfully");
					
					} catch (ArangoDBException e) {
					  System.err.println("Failed to update document. " + e.getMessage());
					  	response.put("success", false);
						response.put("message","Error while cancelling ride. Please try again");
					}
				
			}
		}
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	@POST
	@Path("/confirmreservation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response confirmReservation(
		@FormParam("bookingid") String bookingId) throws JSONException {

		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).getDocument(bookingId,BaseDocument.class);
		
		Map<String, Object> bookingVars;
		String seatsBooked = rideDocument.getAttribute("seats").toString();
		String rideId = rideDocument.getAttribute("rideid").toString();
		rideDocument.updateAttribute("status", "Booking Confirmed");
		
		try {
			arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideDocument.getKey(), rideDocument);
			response.put("success", true);
			response.put("message","Booking Confirmed Successfully");
			
			String bookedBy = rideDocument.getAttribute("booked_by").toString();
			String firebaseId = getFirebaseId(bookedBy);
			
			sendNotification(bookedBy, "The owner has confirmed your reservstion for ride - "+rideId, "Booking Confirmation", firebaseId);
			addNotificationToDB(bookedBy, "Booking Confirmation","The owner has confirmed your reservstion for ride - "+rideId, rideId, firebaseId);
			
			
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  	response.put("success", false);
				response.put("message","Error while cancelling ride. Please try again");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	@POST
	@Path("/startride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response startRide(
		@FormParam("rideid") String rideId) throws JSONException {

		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection(Constants.TABLE_RIDES).getDocument(rideId,BaseDocument.class);
		Map<String, Object> bookingVars;
		
		rideDocument.updateAttribute("status", "Started");
		
		try {
			arangoDB.db(dbName).collection(Constants.TABLE_RIDES).updateDocument(rideId, rideDocument);
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  response.put("success", false);
				response.put("message","Error while starting ride. Please try again");
			}
		
		String rideBookingQuery = "FOR r IN ridebookings FILTER r.rideid == @rideid RETURN r";
		bookingVars = new MapBuilder().put("rideid", rideId).get();
		ArangoCursor<BaseDocument> rideBookingCusor = arangoDB.db(dbName).query(rideBookingQuery, bookingVars, null,
			      BaseDocument.class);
		
		if(rideBookingCusor.hasNext()){
			while(rideBookingCusor.hasNext()){
				BaseDocument rideBookingDocument = rideBookingCusor.next();
				rideBookingDocument.updateAttribute("status", "Ride Started");
				try {
					arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideBookingDocument.getKey(), rideBookingDocument);
					response.put("success", true);
					response.put("message","Ride Started Successfully");
					
					String bookedBy = rideBookingDocument.getAttribute("booked_by").toString();
					String firebaseId = getFirebaseId(bookedBy);
					
					sendNotification(bookedBy, "Ride - "+rideId+ ". Owner has started the ride", "Ride Started", firebaseId);
					addNotificationToDB(bookedBy, "Ride Started", "Ride - "+rideId+ ". Owner has started the ride", rideId, firebaseId);
					
					} catch (ArangoDBException e) {
					  System.err.println("Failed to update document. " + e.getMessage());
					  	response.put("success", false);
						response.put("message","Error while cancelling ride. Please try again");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		}
		else{
			response.put("success", true);
			response.put("message","Ride Started Successfully");
		}
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	@POST
	@Path("/endride")
	@Produces(MediaType.APPLICATION_JSON)
	public Response endRide(
		@FormParam("rideid") String rideId) throws JSONException {

		JSONObject response =  new JSONObject();
		BaseDocument rideDocument = arangoDB.db(dbName).collection(Constants.TABLE_RIDES).getDocument(rideId,BaseDocument.class);
		Map<String, Object> bookingVars;
		
		rideDocument.updateAttribute("status", "Ride Completed");
		
		try {
			arangoDB.db(dbName).collection(Constants.TABLE_RIDES).updateDocument(rideId, rideDocument);
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  response.put("success", false);
				response.put("message","Error while ending the trip. Please try again");
			}
		
		String rideBookingQuery = "FOR r IN ridebookings FILTER r.rideid == @rideid RETURN r";
		bookingVars = new MapBuilder().put("rideid", rideId).get();
		ArangoCursor<BaseDocument> rideBookingCusor = arangoDB.db(dbName).query(rideBookingQuery, bookingVars, null,
			      BaseDocument.class);
		
		if(rideBookingCusor.hasNext()){
			while(rideBookingCusor.hasNext()){
				BaseDocument rideBookingDocument = rideBookingCusor.next();
				rideBookingDocument.updateAttribute("status", "Ride Completed");
				try {
					arangoDB.db(dbName).collection(Constants.TABLE_RIDE_BOOKINGS).updateDocument(rideBookingDocument.getKey(), rideBookingDocument);
					response.put("success", true);
					response.put("message","Ride ended Successfully");
					
					String bookedBy = rideBookingDocument.getAttribute("booked_by").toString();
					String firebaseId = getFirebaseId(bookedBy);
					
					sendNotification(bookedBy, "Ride - "+rideId+ ". Owner has completed the ride", "Ride Completed", firebaseId);
					addNotificationToDB(bookedBy, "Ride Completed", "Ride - "+rideId+ ". Owner has completed the ride", rideId, firebaseId);
					
					} catch (ArangoDBException e) {
					  System.err.println("Failed to update document. " + e.getMessage());
					  	response.put("success", false);
						response.put("message","Error while cancelling ride. Please try again");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		}
		else{
			response.put("success", true);
			response.put("message","Ride Completed Successfully");
		}
		
		return Response.status(200)
			.entity(response)
			.build();
		

	}
	
	public void addNotificationToDB(
		String toEmail,
		String action,
		String message,
		String rideId,
		String firebaseId) {

		System.out.println("Add notification");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date d = new Date();
		System.out.println(dateFormat.format(d)); //2016/11/16 12:08:43
		
		BaseDocument notification = new BaseDocument();
		notification.addAttribute("rideid", rideId);
		notification.addAttribute("sentto", toEmail);
		notification.addAttribute("action", action);
		notification.addAttribute("message", message);
		notification.addAttribute("firebaseid", firebaseId);
		notification.addAttribute("senton", dateFormat.format(d));
		
		
		try {
			  arangoDB.db(dbName).collection("notifications").insertDocument(notification);
			  
			  
			  System.out.println("Notification Added");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to add notification " + e.getMessage());
			  
			}
		
		
	}
	
	
	public Response sendNotification(
		String toEmail,
		String messageToBeSent,
		String action,
		String firebaseId) throws Exception{

		System.out.println("In notif");
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		//post.setHeader("Authorization", "key=AAAAuF96AZM:APA91bG3ImgjUT-GuN55klItTZLJeVgUNR_8KtgYfwVt21Gg3WGBosx25SUw5PvsN2JXw_nbM7xgeFiPZROKjzkB47DZCsZNuWh-COYwIkvPvXYXCw7cS1NGBHy3aZzllCQHW9VmQJa8");
		post.setHeader("Authorization","key="+firebaseServerKey);
		
		JSONObject message = new JSONObject();
		message.put("to", firebaseId);
		message.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", "Viva Carpool" + action);
		notification.put("body", messageToBeSent);

		message.put("data", notification);

		post.setEntity(new StringEntity(message.toString(), "UTF-8"));
		HttpResponse response = client.execute(post);
		System.out.println(response);
		System.out.println(message);
		
		return Response.status(200)
			.entity("addUser")
			.build();

	}
	
	@POST
	@Path("/sendnotification1")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendNotification1() throws Exception{

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		//post.setHeader("Authorization", "key=AAAAuF96AZM:APA91bG3ImgjUT-GuN55klItTZLJeVgUNR_8KtgYfwVt21Gg3WGBosx25SUw5PvsN2JXw_nbM7xgeFiPZROKjzkB47DZCsZNuWh-COYwIkvPvXYXCw7cS1NGBHy3aZzllCQHW9VmQJa8");
		post.setHeader("Authorization","key="+firebaseServerKey);
		
		JSONObject message = new JSONObject();
		//message.put("to", "elK9MIVcZnE:APA91bFPPnebKXmkxf2EAr-cNZEzVscbRuEU4w0H9KFifdppTcigsb5RQHwYyhzczA7BK80e2a7DANXQd4HbSVvNROrDKNq6Hzb3ZgRk9Q2AUxGYzdQN2aGFhLs-RdiNFGf9kTLCq_gp");
		message.put("to", "eHGIjl76UkI:APA91bEgtzKV2DcENyiQNXqbRIr7Mwip1W7u8Tol2rQhZY_2Hy_iVt-R3x6WAJWk-dJxiUM1bGVUXceDH7vt03G_gqKAnDAuoBjncXGIxzhd2F7hbPX-XRJi8qdrZHuYrHkTrW0X9KMT");
		//message.put("to", "dUY_Sk-oxNA:APA91bHRkAqzdMMw72K_glW1eXbuy7nqnz5vs6jlLm7CfvC-JBR1Ut4w57zPLoUmBhRXMAWpovvVnvW6rg85S073HCQWr2djyweOnI49xbZyJZ4tT-5OiYsfPqEFzL6DHd5FfMFFDAhV");
		message.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", "Java");
		notification.put("body", "Notification from Java");

		message.put("data", notification);

		post.setEntity(new StringEntity(message.toString(), "UTF-8"));
		HttpResponse response = client.execute(post);
		System.out.println(response);
		System.out.println(message);
		
		return Response.status(200)
			.entity("addUser")
			.build();

	}
	
	@POST
	@Path("/updatefirebaseid")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateFirebaseId(
			@FormParam("email") String email,
			@FormParam("token") String token,
			@FormParam("type") String type) throws JSONException{
		
		JSONObject response =  new JSONObject();
		BaseDocument document = arangoDB.db(dbName).collection(type).getDocument(email,BaseDocument.class);
		
		System.out.println(document.getKey());
		document.updateAttribute("firebaseid", token);
		
		try {
			arangoDB.db(dbName).collection("dishes").updateDocument(document.getKey(), document);
			response.put("success", true);
			response.put("message","Updated Successfully");
			} catch (ArangoDBException e) {
			  System.err.println("Failed to update document. " + e.getMessage());
			  	response.put("success", false);
				response.put("message","Error while updating. Please try again");
			}
		
		return Response.status(200)
			.entity(response)
			.build();
		
	}
			
			
	@POST
	@Path("/getnotifications")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotification(
		@FormParam("email") String email) {

		JSONObject response =  new JSONObject();
		
		System.out.println(email);
		try {
			
			String query ="";
			Map<String, Object> bindVars, userVars;
			
				query = "FOR r IN notifications FILTER r.sentto == @sentto RETURN r";
				
				bindVars = new MapBuilder().put("sentto", email).get();
				
			ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, bindVars, null,
			      BaseDocument.class);
			
			
			JSONArray notificationsArray = new JSONArray();
			
			if(cursor.hasNext()){
			while(cursor.hasNext()){
			
				JSONObject notification = new JSONObject();
				BaseDocument document = cursor.next();
				
				notification.put("senton", document.getAttribute("senton"));
				notification.put("action", document.getAttribute("action"));
				notification.put("rideid", document.getAttribute("rideid"));
				notification.put("message", document.getAttribute("message"));
				
				notificationsArray.put(notification);
					
			}
			response.put("success", true);
			response.put("message","Ride Present");
			response.put("notifications", notificationsArray);
			
			}
			else{
				response.put("success", false);
				response.put("message","Ride Not present");
				response.put("notifications", notificationsArray);
			}
				  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/getpaymenthistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPaymentHistory(
		@FormParam("email") String email) {

		
		String userKey = getUserKey(email);
		
		BaseDocument user = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(userKey,BaseDocument.class);
		
		String custId = user.getAttribute("stripeid").toString();
		
		List<Charge> listOfCharges = StripeServices.getPaymentHistory(custId);
		
		JSONArray chargesArray = new JSONArray();
		Iterator<Charge> iter = listOfCharges.iterator();
		
		while(iter.hasNext()){
			Charge charge = iter.next();
			JSONObject chargeObject = new JSONObject();
			try {
				chargeObject.put("amount", "A$" + charge.getAmount()/100);
				
				chargeObject.put("description", charge.getDescription());
				chargeObject.put("status", charge.getStatus());
				Date d = new Date(charge.getCreated() * 1000);
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
				chargeObject.put("date", sdf.format(d));
				Card c = (Card)charge.getSource();
				System.out.println(c.getLast4());
				chargeObject.put("card", "xxxx xxxx xxxx "+c.getLast4());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			chargesArray.put(chargeObject);
		}
		JSONObject response =  new JSONObject();
		
		try {
			 
			  try {
					response.put("success", true);
					response.put("message","Ride Booked Successfully");
					response.put("charges", chargesArray);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			} catch (ArangoDBException e) {
			  System.err.println("Failed to get Payment History " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error while booking ride. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	@POST
	@Path("/getcarddetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCardDetails(
		@FormParam("email") String email) {

		String userKey = getUserKey(email);
		
		BaseDocument user = arangoDB.db(dbName).collection(Constants.TABLE_USERS).getDocument(userKey,BaseDocument.class);
		
		String custId = user.getAttribute("stripeid").toString();
		
		Card card = StripeServices.getCardDetails(custId);
		
		JSONArray cardsArray = new JSONArray();
		JSONObject cardObject = new JSONObject();
		
		JSONObject response =  new JSONObject();
		
		try {
			 cardObject.put("number", "xxxx xxxx xxxx "+card.getLast4());
			 cardObject.put("expiry", card.getExpMonth()+"/"+card.getExpYear());
			 cardObject.put("type", card.getType());
			 cardObject.put("cvc", card.getCvcCheck());
			 cardsArray.put(cardObject);
			  try {
					response.put("success", true);
					response.put("message","Ride Booked Successfully");
					response.put("cards", cardsArray);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			} catch (ArangoDBException e) {
			  System.err.println("Failed to create document. " + e.getMessage());
			  
				try {
					response.put("success", false);
					response.put("message","Error while booking ride. Please try again");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		
		return Response.status(200)
			.entity(response)
			.build();

	}
	
	public String getUserKey(String email){
		
			try {
					
					String query ="";
					Map<String, Object> userVars;
					
						query = "FOR u IN users FILTER u.email == @email RETURN u";
						
						userVars = new MapBuilder().put("email", email).get();
						
					
					
					ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, userVars, null,
					      BaseDocument.class);
					
					
					
					if(cursor.hasNext()){
						while(cursor.hasNext()){
						
							
							BaseDocument document = cursor.next();
							return document.getKey();
								
						}
					}
					else{
						return null;
					}
						  
				
				} catch (ArangoDBException e) {
				  System.err.println("Failed to execute query. " + e.getMessage());
				} 
			
			return null;
	}
	
	public String getFirebaseId(String email){
		
		try {
				
				String query ="";
				Map<String, Object> userVars;
				
					query = "FOR u IN users FILTER u.email == @email RETURN u";
					
					userVars = new MapBuilder().put("email", email).get();
					
				
				
				ArangoCursor<BaseDocument> cursor = arangoDB.db(dbName).query(query, userVars, null,
				      BaseDocument.class);
				
				
				
				if(cursor.hasNext()){
					while(cursor.hasNext()){
					
						
						BaseDocument document = cursor.next();
						return document.getAttribute("firebaseid").toString();
							
					}
				}
				else{
					return null;
				}
					  
			
			} catch (ArangoDBException e) {
			  System.err.println("Failed to execute query. " + e.getMessage());
			} 
		
		return null;
}
}